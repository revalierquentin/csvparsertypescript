import { number } from "yargs";

const csv = require('csv-parser');
const fs = require('fs');
const https = require('https');
const decompress = require("decompress");
let nbTransfertSiege: number = 0;
let nbTotal: number = 0;

async function downloadFile() {
    return new Promise((resolve, reject) => {
        try {
            if (fs.existsSync("./csv/StockEtablissementLiensSuccession_utf8.csv")) {
                console.log("Le fichier existe déjà !");
                resolve(true);
            } else {
                const file = fs.createWriteStream("StockEtablissementLiensSuccession_utf8.zip");
                console.log("Début du téléchargement !");
                const request = https.get("https://files.data.gouv.fr/insee-sirene/StockEtablissementLiensSuccession_utf8.zip", function (response: any) {
                    response.pipe(file);
                    file.on("finish", () => {
                        file.close();
                        console.log("Fin du téléchargement !");
                        decompress("StockEtablissementLiensSuccession_utf8.zip", "csv")
                            .then((files: any) => {
                                console.log(files);
                                console.log("Le fichier a bien été dézippé !");
                                resolve(true);
                            })
                            .catch((error: any) => {
                                console.error(error);
                                reject(error);
                            });
                    });
                });
            }
        } catch (error) {
            console.error(error);
            reject(error);
        }
    });
}

async function getPercentage() {
    return new Promise((resolve, reject) => {
        try {
            fs.createReadStream('csv/StockEtablissementLiensSuccession_utf8.csv')
                .pipe(csv())
                .on('data', (data: any) => {
                    nbTotal += 1;
                    nbTransfertSiege += getNbTransfertSiegeTrue(data);
                    console.log(calculPercentage(8150337, nbTotal) + "%");
                })
                .on('end', () => {
                    console.log("OK");
                    console.log(calculPercentage(nbTotal, nbTransfertSiege));
                    resolve(calculPercentage(nbTotal, nbTransfertSiege) + "%");
                });
        } catch (error) {
            console.error(error);
            reject(error);
        }
    });
}

function getNbTransfertSiegeTrue(data: any) {
    if (data["transfertSiege"] == "true") {
        return 1;
    }
    return 0;
}

function calculPercentage(total: number, numOfElems: number) {
    return parseFloat((numOfElems / total * 100).toFixed(2));
}

module.exports = { getPercentage, downloadFile, getNbTransfertSiegeTrue, calculPercentage };