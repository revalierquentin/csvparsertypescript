const parser = require('./parser');

let trueData: any = {
    siretEtablissementPredecesseur: '40825451400024',
    siretEtablissementSuccesseur: '40825451400032',
    dateLienSuccession: '2015-02-11',
    transfertSiege: 'true',
    continuiteEconomique: 'false',
    dateDernierTraitementLienSuccession: '2015-03-25T15:09:45'
};

let falseData: any = {
    siretEtablissementPredecesseur: '40825451400024',
    siretEtablissementSuccesseur: '40825451400032',
    dateLienSuccession: '2015-02-11',
    transfertSiege: 'false',
    continuiteEconomique: 'false',
    dateDernierTraitementLienSuccession: '2015-03-25T15:09:45'
};

describe("Parser checker", () => {
    describe("Function getNbTransfertSiegeTrue(data)", () => {
        test("Check getNbTransfertSiegeTrue(data) => 1 ", () => {
            expect(parser.getNbTransfertSiegeTrue(trueData)).toEqual(1);
        });
        test("Check getNbTransfertSiegeTrue(data) => 0 ", () => {
            expect(parser.getNbTransfertSiegeTrue(falseData)).toEqual(0);
        });
    });
    describe("Function calculPercentage(total, numOfElems)", () => {
        test("Check calculPercentage(total, numOfElems) => 25.00 ", () => {
            expect(parser.calculPercentage(20, 5)).toEqual(25);
        });
        test("Check calculPercentage(total, numOfElems) => 10.00 ", () => {
            expect(parser.calculPercentage(100, 10)).toEqual(10);
        });
        test("Check calculPercentage(total, numOfElems) => 10.00 ", () => {
            expect(parser.calculPercentage(37, 8)).toEqual(21.62);
        });
    });
});

export {};