var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const parser = require('./parser');
const express = require('express');
const app = express();
const port = 3000;
app.get('/', (req, res) => {
    res.send('Bienvenue sur votre Parser ! <br><br> Rendez-vous <a href="http://localhost:' + port + '/tp1">ici</a> pour obtenir le pourcentage souhaité !');
});
app.get('/tp1', function (req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        parser.downloadFile().then((response) => {
            if (response == true) {
                parser.getPercentage().then((response) => {
                    res.send(200, response);
                });
            }
            else {
                res.send(500, "Une erreur est survenue !");
            }
        });
    });
});
app.listen(port, () => {
    console.log(`Parser app listening on http://localhost:${port}`);
});
//# sourceMappingURL=index.js.map