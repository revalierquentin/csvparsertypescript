"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const csv = require('csv-parser');
const fs = require('fs');
const https = require('https');
const decompress = require("decompress");
let nbTransfertSiege = 0;
let nbTotal = 0;
function downloadFile() {
    return __awaiter(this, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            try {
                if (fs.existsSync("./csv/StockEtablissementLiensSuccession_utf8.csv")) {
                    console.log("Le fichier existe déjà !");
                    resolve(true);
                }
                else {
                    const file = fs.createWriteStream("StockEtablissementLiensSuccession_utf8.zip");
                    console.log("Début du téléchargement !");
                    const request = https.get("https://files.data.gouv.fr/insee-sirene/StockEtablissementLiensSuccession_utf8.zip", function (response) {
                        response.pipe(file);
                        file.on("finish", () => {
                            file.close();
                            console.log("Fin du téléchargement !");
                            decompress("StockEtablissementLiensSuccession_utf8.zip", "csv")
                                .then((files) => {
                                console.log(files);
                                console.log("Le fichier a bien été dézippé !");
                                resolve(true);
                            })
                                .catch((error) => {
                                console.error(error);
                                reject(error);
                            });
                        });
                    });
                }
            }
            catch (error) {
                console.error(error);
                reject(error);
            }
        });
    });
}
function getPercentage() {
    return __awaiter(this, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            try {
                fs.createReadStream('csv/StockEtablissementLiensSuccession_utf8.csv')
                    .pipe(csv())
                    .on('data', (data) => {
                    nbTotal += 1;
                    nbTransfertSiege += getNbTransfertSiegeTrue(data);
                    console.log(calculPercentage(8150337, nbTotal) + "%");
                })
                    .on('end', () => {
                    console.log("OK");
                    console.log(calculPercentage(nbTotal, nbTransfertSiege));
                    resolve(calculPercentage(nbTotal, nbTransfertSiege) + "%");
                });
            }
            catch (error) {
                console.error(error);
                reject(error);
            }
        });
    });
}
function getNbTransfertSiegeTrue(data) {
    if (data["transfertSiege"] == "true") {
        return 1;
    }
    return 0;
}
function calculPercentage(total, numOfElems) {
    return parseFloat((numOfElems / total * 100).toFixed(2));
}
module.exports = { getPercentage, downloadFile, getNbTransfertSiegeTrue, calculPercentage };
//# sourceMappingURL=parser.js.map